local cmp = require("cmp")
local cmp_media = require("cmp-media")
cmp.register_source("media", cmp_media.new())
