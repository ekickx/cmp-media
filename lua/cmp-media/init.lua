local cmp = require("cmp")
local source = {}

local defaults = {
  --[[
  -- - change root dir/cwd. By default relative to current files.
  --   But can be set with and api. Can be used with other plugin. Nerdtree for example
  -- - support various filetype snippet:
  --   ```
  --   markdown:
  --    prefix: "![]('", suffix: "')"
  --   ```
  --]]
}

function source.new()
  return setmetatable({}, {__index = source})
end

function source.get_trigger_characters()
  return {"!"}
end

function source.complete(self, request, callback)
  local opt = request.option

  local files = vim.fn.system("find . -name '*.png'")
  files = vim.fn.split(files, "\n")

  files = vim.tbl_map(function(file)
    return {
      label = file,
      insertText = ('[]("%s")'):format(file),
    }
  end, files)

  callback{items = files, isIncomplete = true}
end

return source
